## DemoProject

`This project is very simple and is intended solely to demonstrate an understanding of the experience of working out with React.js.`


## Installation

1. For starting project, you need to install node.js version 10.15 or higher. nodejs.org
2. Install node modules: *`npm install`


## Development
*`npm run start`

The project will work on port 3008 (http://localhost:3008/)


## Release
*`npm run build`

After, in dist/` must be:

- index.html
- bundle.js
- style.css

This is files need to push on the server.


Now for API used this service: `https://next.json-generator.com/api/json/get/E1P8IkqG8`
