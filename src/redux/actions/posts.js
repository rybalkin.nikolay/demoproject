import axios from 'axios';

import { baseURL } from '../../constants/api';

export const GET_POSTS = 'GET_POSTS';

export const getPosts = (posts, pageNumber) => ({
  type: GET_POSTS,
  posts,
  pageNumber,
});

export const fetchPosts = (id, pageNumber) => (dispatch) => axios.get(baseURL).
then(({ data }) => {
  dispatch(getPosts(data, pageNumber));
}).
catch((data) => {
  console.error(`Error response! ${data}`);
});
