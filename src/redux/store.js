import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import persistState from 'redux-localstorage';
import rootReducer from './reducers';

import { init, emit } from './websockets';

const getMiddleware = () => {
  const middleware = [
    thunkMiddleware.withExtraArgument({ emit }),
  ];

  return applyMiddleware(...middleware);
};

const configureStore = () => compose(
  getMiddleware(),
  persistState(),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
)(createStore)(rootReducer);

const store = configureStore();

init(store);

export default store;
