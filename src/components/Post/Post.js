import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import moment from 'moment';
import Avatar from '@material-ui/core/Avatar';
import Forward from '@material-ui/icons/Forward';

import './styles.less';

export class Post extends Component {
  state = {
    showAll: false,
  };

  handleShowAllText = () => {
    this.setState({
      showAll: true,
    });
  };

  render() {
    const { data } = this.props;
    const { showAll } = this.state;

    const dateData = data.date;
    const descriptionData = data.description;

    const isLongString = descriptionData.split(' ').join().split('').length >= 700;
    const content = isLongString && !showAll ? `${descriptionData.substr(0, 700)}... ` : descriptionData;

    const date = `${moment(dateData).fromNow(true)}: ${moment(dateData).format('HH:mm A')}`;
    const fixedText = content ? content.split('\n') : [];

    return (
      <div className="event">
        <div className="head flex between">
          { data.title }
        </div>
        <div className="author-data flex between">
          <div className="flex left">
            <Avatar
              alt="Marie de Cordier"
              className="avatar"
              src={data.user.icon}
            />
            <div className="title">
              { data.user.name }
            </div>
          </div>
          <div className="date">
            { date }
          </div>
        </div>
        <div className="content">
            <span>
              {
                fixedText.map((item, index) => (
                  <span
                    key={index}
                  >
                    { item }
                    {
                      isLongString && !showAll && fixedText.length - 1 === index &&
                      <span
                        className="more"
                        onClick={this.handleShowAllText}
                      >
                        { 'more' }
                      </span>
                    }
                  </span>
                ))
              }
            </span>
        </div>
      </div>
    );
  }
}
