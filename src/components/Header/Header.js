import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';

import Avatar from '@material-ui/core/Avatar';

import './styles.less';

export class Header extends Component {
  render() {
    return (
      <div className="header flex between">
        <div className="logo flex">
          <span>
            {'Logo'}
          </span>
        </div>
        <div className="menu flex">
          <NavLink
            activeClassName="active"
            className="item"
            exact
            to="/"
          >
            {'Home'}
          </NavLink>
          <NavLink
            activeClassName="active"
            className="item"
            exact
            to="/table"
          >
            {'Table'}
          </NavLink>
          <NavLink
            activeClassName="active"
            className="item"
            exact
            to="/posts"
          >
            {'Posts'}
          </NavLink>
        </div>
        <Avatar
          alt="Marie de Cordier"
          className="avatar"
          src="http://placehold.it/40x40"
        />
      </div>
    );
  }
}
