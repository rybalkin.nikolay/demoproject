import React, { Fragment } from 'react';

import './styles.less';

export const Home = () =>{
  return (
    <Fragment>
      <div className="title flex">
        Welcome to the demo project.
      </div>
      <div className="description flex">
        <div className="main-info">
          {'This project is very simple and is intended solely to demonstrate an understanding of the experience of working out with  React.js.'}
        </div>
        <div className="other-info">
          {'You can see the source code here: '}
          <span>
            <a
              href="https://gitlab.com/rybalkin.nikolay/demoproject"
              target="_blank"
            >
              {'gitlab.com/rybalkin.nikolay/demoproject'}
            </a>
          </span>
        </div>
      </div>
    </Fragment>
  )
};
