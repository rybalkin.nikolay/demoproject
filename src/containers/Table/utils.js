const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

export const generateData = () => {
  const data = [];

  for(let i = 0; i <= 300; i++){
    data.push({
      id: `${i}`,
      type: `type-${possible.charAt(Math.floor(Math.random() * possible.length))}`,
      name: `name-${possible.charAt(Math.floor(Math.random() * possible.length))}`,
      warning: Math.random() >= 0.8,
      players: Math.floor(Math.random() * 7),
      maxPlayers: Math.floor(Math.random() * 13),
    })
  }

  return data;
};
