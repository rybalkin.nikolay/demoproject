import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  Switch,
  Route,
} from 'react-router-dom';
import * as classNames from 'classnames';

import { Header } from '../../components/Header/Header';
import { Home } from '../Home/Home';
import { TableComponents } from '../Table/Table';
import { PostsComponents } from '../Posts/Posts';

import './styles.less';

export class App extends Component {

  render() {
    const currentLocation = location.pathname.replace('/', '');
    const classesWrapper = currentLocation === '' ? 'home' : currentLocation;
    const appClassNames = classNames(
      'app-wrapper',
      [classesWrapper]
    );

    return (
      <div className={appClassNames}>
        <Header />
        <div className="content-wrapper flex left top">
          <div className="content-block">
            <Switch>
              <Route
                component={Home}
                exact
                path="/"
              />
              <Route
                component={TableComponents}
                exact
                path="/table"
              />
              <Route
                component={PostsComponents}
                exact
                path="/posts"
              />
            </Switch>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {};

const mapDispatchToProps = (dispatch) => ({});

const AppComponents = connect(mapStateToProps, mapDispatchToProps)(App);

export { AppComponents };
