import React, { Component } from 'react';
import { connect } from 'react-redux';

import { Post } from '../../components/Post/Post';

import { fetchPosts } from '../../redux/actions/posts';

import './styles.less';

export class Posts extends Component{
  componentDidMount() {
    this.props.fetchPostsAction()
  }

  render() {
    const { postsData } = this.props;

    return (
      <div className="posts-content">
        <div className="top-block">
          <div className="description">
            {'This page demonstrate working with API.'}
            <br />
            {'This service is used as api: https://next.json-generator.com/'}
          </div>
        </div>
        <div className="list-posts">
          {
            postsData.map((item) => (
              <Post
                key={item.id}
                data={item}
              />
            ))
          }
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  const { postsData } = state;
  console.log(postsData);

  return {
    postsData: postsData || [],
  };
};

const mapDispatchToProps = (dispatch) => ({
  fetchPostsAction: () => dispatch(fetchPosts()),
});

const PostsComponents = connect(mapStateToProps, mapDispatchToProps)(Posts);

export { PostsComponents };

