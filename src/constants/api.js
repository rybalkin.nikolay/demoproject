export const baseURL = 'https://next.json-generator.com/api/json/get/E1P8IkqG8';

export const getEventsParams = () => `${baseURL}/events`;
export const getMembersParams = () => `${baseURL}/members`;
export const getPostsParams = () => `${baseURL}/posts`;

export const uri = 'wss://js-assignment.demo.com/ws_api';

export const actionTypes = {
  ping: 'ping',
  login: 'login',
  subscribe_tables: 'subscribe_tables',
  add_table: 'add_table',
  update_table: 'update_table',
  remove_table: 'remove_table',
};

